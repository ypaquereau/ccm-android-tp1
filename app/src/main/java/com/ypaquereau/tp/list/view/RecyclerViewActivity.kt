package com.ypaquereau.tp.list.view

import android.os.Bundle
import android.view.HapticFeedbackConstants
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ypaquereau.tp.databinding.ActivityRecyclerViewBinding
import com.ypaquereau.tp.list.model.*
import com.ypaquereau.tp.list.viewmodel.DeviceViewModel

class RecyclerViewActivity: AppCompatActivity() {

    private lateinit var adapter: DeviceAdapter
    private lateinit var binding: ActivityRecyclerViewBinding
    private lateinit var viewModel: DeviceViewModel

    private val deviceListObserver = Observer<List<MyObjectForRecyclerView>> {
        adapter.submitList(it)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRecyclerViewBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(this)[DeviceViewModel::class.java]

        // Create the instance of adapter
        adapter = DeviceAdapter { item, view ->
            onItemClick(item, view)
        }

        // We define the style
        binding.recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)

        // We set the adapter to recycler view
        binding.recyclerView.adapter = adapter
    }

    private fun onItemClick(objectDataSample: ObjectDataSample, view : View) {
        view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
        Toast.makeText(this, "${objectDataSample.deviceConstructor} ${objectDataSample.deviceModel}", Toast.LENGTH_LONG).show()
    }
}

enum class MyItemType(val type: Int) {
    ROW(0),
    HEADER(1),
    FOOTER(2)
}