package com.ypaquereau.tp.list.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import com.ypaquereau.tp.architecture.CustomApplication
import com.ypaquereau.tp.list.model.*

class DeviceRepository {
    private val mDeviceDao = CustomApplication.instance.mApplicationDatabase.mDeviceDao()

    fun selectAllDevice(): LiveData<List<ObjectDataSample>> {
        return mDeviceDao.selectAll().map { list ->
            list.toObjectDataSample()
        }
    }


    fun insertDevice(objectDataSample: ObjectDataSample) {
        mDeviceDao.insert(objectDataSample.toRoomObject())
    }


    fun deleteAllDevice() {
        mDeviceDao.deleteAll()
    }

    private fun ObjectDataSample.toRoomObject(): ObjectDataSample {
        return ObjectDataSample(
            deviceConstructor = deviceConstructor,
            deviceModel = deviceModel,
            deviceType = deviceType,
            devicePrice = devicePrice,
            deviceImage = deviceImage
        )
    }


    private fun List<ObjectDataSample>.toObjectDataSample(): List<ObjectDataSample> {
        return map { eachItem ->
            ObjectDataSample(
                deviceConstructor = eachItem.deviceConstructor,
                deviceModel = eachItem.deviceModel,
                deviceType = eachItem.deviceType,
                devicePrice = eachItem.devicePrice,
                deviceImage = eachItem.deviceImage
            )
        }
    }


    /*
    fun generateFakeData(): MutableList<MyObjectForRecyclerView> {
        val result = mutableListOf<MyObjectForRecyclerView>()

        mutableListOf(
            ObjectDataSample("https://fscl01.fonpit.de/devices/82/1482.png", "Xiaomi", "Mi 9T Pro", 399.99, DeviceType.PHONE),
            ObjectDataSample("https://images.frandroid.com/wp-content/uploads/2020/01/samsung-galaxy-s20-frandroid-2020-5g.png", "Samsung", "Galaxy S20", 699.99, DeviceType.PHONE),
            ObjectDataSample("https://c0.lestechnophiles.com/images.frandroid.com/wp-content/uploads/2021/09/apple-iphone-13-frandroid-2021.png", "Apple", "iPhone 13", 1299.99, DeviceType.PHONE),
            ObjectDataSample("https://c2.lestechnophiles.com/images.frandroid.com/wp-content/uploads/2021/09/ipad-mini-6-2021-frandroid.png", "Apple", "iPad Mini", 559.00, DeviceType.TABLET),
        ).groupBy {
            it.deviceType
        }.forEach { (deviceType, items) ->
            result.add(ObjectDataHeaderSample(deviceType.type))
            result.addAll(items)
            result.add(ObjectDataFooterSample("${items.size} ${deviceType.type}"))
        }

        return result
    }*/
}