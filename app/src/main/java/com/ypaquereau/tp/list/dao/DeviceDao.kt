package com.ypaquereau.tp.list.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ypaquereau.tp.list.model.ObjectDataSample

@Dao
interface DeviceDao {
    @Query("SELECT * FROM device_object_table ORDER BY devicePrice DESC")
    fun selectAll(): LiveData<List<ObjectDataSample>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(device: ObjectDataSample)

    @Query("DELETE FROM device_object_table")
    fun deleteAll()
}