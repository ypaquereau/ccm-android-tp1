package com.ypaquereau.tp.list.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ypaquereau.tp.R
import com.ypaquereau.tp.databinding.ItemCustomRecyclerBinding
import com.ypaquereau.tp.databinding.ItemCustomRecyclerFooterBinding
import com.ypaquereau.tp.databinding.ItemCustomRecyclerHeaderBinding
import com.ypaquereau.tp.list.model.MyObjectForRecyclerView
import com.ypaquereau.tp.list.model.ObjectDataFooterSample
import com.ypaquereau.tp.list.model.ObjectDataHeaderSample
import com.ypaquereau.tp.list.model.ObjectDataSample
import java.lang.RuntimeException

private val diffItemUtils = object : DiffUtil.ItemCallback<MyObjectForRecyclerView>() {

    override fun areItemsTheSame(oldItem: MyObjectForRecyclerView, newItem: MyObjectForRecyclerView): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: MyObjectForRecyclerView, newItem: MyObjectForRecyclerView): Boolean {
        return oldItem == newItem
    }
}

class DeviceAdapter(
    private val onItemClick: (quoteUi: ObjectDataSample, view: View) ->Unit
) : ListAdapter<MyObjectForRecyclerView, RecyclerView.ViewHolder>(diffItemUtils) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        when (viewType) {
            MyItemType.ROW.type -> {
                DeviceViewHolder(
                    ItemCustomRecyclerBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    ), onItemClick
                )
            }
            MyItemType.HEADER.type -> {
                DeviceHeaderViewHolder(
                    ItemCustomRecyclerHeaderBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            }
            MyItemType.FOOTER.type -> {
                DeviceFooterViewHolder(
                    ItemCustomRecyclerFooterBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            }
            else -> throw RuntimeException("Wrong view type received $viewType")
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) =
        when (holder.itemViewType) {
            MyItemType.ROW.type -> (holder as DeviceViewHolder).bind(getItem(position) as ObjectDataSample)
            MyItemType.HEADER.type -> (holder as DeviceHeaderViewHolder).bind(getItem(position) as ObjectDataHeaderSample)
            MyItemType.FOOTER.type -> (holder as DeviceFooterViewHolder).bind(getItem(position) as ObjectDataFooterSample)
            else -> throw RuntimeException("Wrong view type received ${holder.itemView}")
        }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is ObjectDataSample -> MyItemType.ROW.type
            is ObjectDataHeaderSample -> MyItemType.HEADER.type
            is ObjectDataFooterSample -> MyItemType.FOOTER.type
        }
    }
}

class DeviceViewHolder(
    private val binding: ItemCustomRecyclerBinding,
    onItemClick: (objectDataSample: ObjectDataSample, view: View) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    private lateinit var ui: ObjectDataSample

    init {
        binding.root.setOnClickListener {
            onItemClick(ui, itemView)
        }
    }

    fun bind(objectDataSample: ObjectDataSample) {
        ui = objectDataSample

        Glide.with(itemView.context)
            .load(objectDataSample.deviceImage)
            .placeholder(R.drawable.ic_launcher_background)
            .into(binding.itemRecyclerViewPhoneImage)
        binding.itemRecyclerViewPhoneConstructor.text = objectDataSample.deviceConstructor
        binding.itemRecyclerViewPhoneModel.text = objectDataSample.deviceModel
        binding.itemRecyclerViewPhonePrice.text = "${objectDataSample.devicePrice} €"
    }
}

class DeviceHeaderViewHolder(
    private val binding: ItemCustomRecyclerHeaderBinding
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(objectDataHeaderSample: ObjectDataHeaderSample) {
        binding.itemRecyclerViewHeader.text = objectDataHeaderSample.headerText
    }
}

class DeviceFooterViewHolder(
    private val binding: ItemCustomRecyclerFooterBinding
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(objectDataFooterSample: ObjectDataFooterSample) {
        binding.itemRecyclerViewFooter.text = objectDataFooterSample.headerText
    }
}