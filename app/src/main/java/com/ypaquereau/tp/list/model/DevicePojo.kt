package com.ypaquereau.tp.list.model

import androidx.room.Entity
import androidx.room.PrimaryKey

sealed class MyObjectForRecyclerView()

@Entity(tableName = "device_object_table")
data class ObjectDataSample(
    val deviceImage: String,
    val deviceConstructor: String,
    val deviceModel: String,
    val devicePrice: Double,
    val deviceType: DeviceType
) : MyObjectForRecyclerView() {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}

data class ObjectDataHeaderSample(
    val headerText : String
) : MyObjectForRecyclerView()

data class ObjectDataFooterSample(
    val headerText : String
) : MyObjectForRecyclerView()

enum class DeviceType(val type: String) {
    PHONE("Phones"),
    TABLET("Tablets")
}