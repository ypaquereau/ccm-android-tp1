package com.ypaquereau.tp.list.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ypaquereau.tp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.mainButtonActivityList.setOnClickListener {
            goToActivityIntent()
        }
    }

    /**
     * Redirect to the list
     */
    private fun goToActivityIntent() {
        startActivity(Intent(this, RecyclerViewActivity::class.java))
    }
}