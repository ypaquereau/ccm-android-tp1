package com.ypaquereau.tp.list.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.ypaquereau.tp.list.model.*
import com.ypaquereau.tp.list.repository.DeviceRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DeviceViewModel: ViewModel() {
    private val deviceRepository: DeviceRepository by lazy { DeviceRepository() }
    private val deviceList: LiveData<List<MyObjectForRecyclerView>> =
        deviceRepository.selectAllDevice().map { list ->
            list.toMyObjectForRecyclerView()
        }

    fun insertDevice(deviceConstructor: String, deviceModel: String, deviceType: DeviceType, devicePrice: Double, deviceImage: String) {
        viewModelScope.launch(Dispatchers.IO) {
            deviceRepository.insertDevice(
                ObjectDataSample(deviceImage, deviceConstructor, deviceModel, devicePrice, deviceType)
            )
        }
    }

    private fun List<ObjectDataSample>.toMyObjectForRecyclerView(): List<MyObjectForRecyclerView> {
        val result = mutableListOf<MyObjectForRecyclerView>()

        groupBy {
            it.deviceType
        }.forEach { (deviceType, items) ->
            result.add(ObjectDataHeaderSample(deviceType.type))
            result.addAll(items)
            result.add(ObjectDataFooterSample("${items.size} ${deviceType.type}"))
        }
        return result
    }

}