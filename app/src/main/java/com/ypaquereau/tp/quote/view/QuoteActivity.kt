package com.ypaquereau.tp.quote.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ypaquereau.tp.quote.model.QuotesUi
import com.ypaquereau.tp.quote.viewmodel.QuoteViewModel
import com.ypaquereau.tp.databinding.ActivityQuoteBinding

class QuoteActivity : AppCompatActivity() {
    private lateinit var viewModel: QuoteViewModel
    private lateinit var binding : ActivityQuoteBinding
    private val adapter : QuoteAdapter = QuoteAdapter()
    private val observer = Observer<List<QuotesUi>> {
        adapter.submitList(it)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityQuoteBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(this)[QuoteViewModel::class.java]

        binding.chuckNorrisActivityRv.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        binding.chuckNorrisActivityRv.adapter = adapter

        binding.chuckNorrisActivityAdd.setOnClickListener {
            viewModel.fetchNewQuote()
        }

        binding.chuckNorrisActivityDelete.setOnClickListener {
            viewModel.deleteAll()
        }
    }
    override fun onStart() {
        super.onStart()
        viewModel.mQuotesQuoteLiveData.observe(this, observer)
    }

    override fun onStop() {
        viewModel.mQuotesQuoteLiveData.removeObserver(observer)
        super.onStop()
    }
}