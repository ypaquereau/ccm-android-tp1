package com.ypaquereau.tp.quote.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ypaquereau.tp.quote.model.FactsRoom

@Dao
interface QuotesDao {
    @Query("SELECT * FROM quotes")
    fun selectAll() : LiveData<List<FactsRoom>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(factsRoom: FactsRoom)

    @Query("DELETE FROM quotes")
    fun deleteAll()
}
