package com.ypaquereau.tp.quote.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ypaquereau.tp.quote.model.QuotesUi
import com.ypaquereau.tp.databinding.ItemQuoteBinding

val diffUtils = object : DiffUtil.ItemCallback<QuotesUi>() {
    override fun areItemsTheSame(oldItem: QuotesUi, newItem: QuotesUi): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: QuotesUi, newItem: QuotesUi): Boolean {
        return oldItem == newItem
    }
}

class QuoteViewHolder(
    private val binding: ItemQuoteBinding
) : RecyclerView.ViewHolder(binding.root) {

    private lateinit var ui: QuotesUi

    fun bind(quotesUi: QuotesUi) {
        ui = quotesUi
        binding.itemQuote.text = quotesUi.quote
    }
}

class QuoteAdapter : ListAdapter<QuotesUi, QuoteViewHolder>(diffUtils) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuoteViewHolder {
        return QuoteViewHolder(
            ItemQuoteBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: QuoteViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}