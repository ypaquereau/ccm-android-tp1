package com.ypaquereau.tp.quote.repository

import androidx.lifecycle.LiveData
import com.ypaquereau.tp.architecture.CustomApplication
import com.ypaquereau.tp.architecture.RetrofitBuilder
import com.ypaquereau.tp.quote.model.QuotesRetrofit
import com.ypaquereau.tp.quote.model.FactsRoom
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class QuotesRepository {
    private val mQuotesDao = CustomApplication.instance.mApplicationDatabase.quotesDao()

    fun selectAllQuote(): LiveData<List<FactsRoom>> {
        return mQuotesDao.selectAll()
    }

    private suspend fun insertQuote(factsQuote: FactsRoom) =
        withContext(Dispatchers.IO) {
            mQuotesDao.insert(factsQuote)
        }

    suspend fun deleteAllQuote() = withContext(Dispatchers.IO) {
        mQuotesDao.deleteAll()
    }

    suspend fun fetchData() {
        insertQuote(RetrofitBuilder.getQuote().getRandomQuote().toRoom())
    }
}

private fun QuotesRetrofit.toRoom(): FactsRoom {
    return FactsRoom(
        quote = quote
    )
}