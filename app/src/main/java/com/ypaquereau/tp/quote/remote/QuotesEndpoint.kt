package com.ypaquereau.tp.quote.remote

import com.ypaquereau.tp.quote.model.QuotesRetrofit
import retrofit2.http.GET

interface QuotesEndpoint {
    @GET("random")
    suspend fun getRandomQuote() : QuotesRetrofit
}