package com.ypaquereau.tp.quote.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.ypaquereau.tp.quote.model.FactsRoom
import com.ypaquereau.tp.quote.model.QuotesUi
import com.ypaquereau.tp.quote.repository.QuotesRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class QuoteViewModel : ViewModel() {
    private val mQuotesRepository: QuotesRepository by lazy { QuotesRepository() }
    var mQuotesQuoteLiveData: LiveData<List<QuotesUi>> =
        mQuotesRepository.selectAllQuote().map {
            it.toUi()
        }

    fun fetchNewQuote() {
        viewModelScope.launch(Dispatchers.IO) {
            mQuotesRepository.fetchData()
        }
    }

    fun deleteAll() {
        viewModelScope.launch(Dispatchers.IO) {
            mQuotesRepository.deleteAllQuote()
        }
    }
}

private fun List<FactsRoom>.toUi(): List<QuotesUi> {
    return asSequence().map {
        QuotesUi(
            quote = it.quote
        )
    }.toList()
}