package com.ypaquereau.tp.quote.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/** Object use for room */
@Entity(tableName = "quotes")
data class FactsRoom(
    @ColumnInfo(name = "quote_text")
    val quote: String,
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}

/** Object use for Ui */
data class QuotesUi(
    val quote: String,
)

/** Object use for retrofit */
data class QuotesRetrofit(
    @Expose
    @SerializedName("text")
    val quote: String,
)