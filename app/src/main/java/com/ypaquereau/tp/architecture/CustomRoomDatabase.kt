package com.ypaquereau.tp.architecture

import androidx.room.Database
import androidx.room.RoomDatabase
import com.ypaquereau.tp.quote.dao.QuotesDao
import com.ypaquereau.tp.quote.model.FactsRoom
import com.ypaquereau.tp.list.dao.DeviceDao
import com.ypaquereau.tp.list.model.ObjectDataSample

@Database(
    entities = [
        ObjectDataSample::class,
        FactsRoom::class
    ],
    version = 3,
    exportSchema = false
)

abstract class CustomRoomDatabase: RoomDatabase() {
    abstract fun mDeviceDao(): DeviceDao
    abstract fun quotesDao(): QuotesDao
}