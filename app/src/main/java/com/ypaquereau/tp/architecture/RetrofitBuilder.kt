package com.ypaquereau.tp.architecture

import com.google.gson.GsonBuilder
import com.ypaquereau.tp.quote.remote.QuotesEndpoint
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitBuilder {
    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl("https://cat-fact.herokuapp.com/facts/")
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()))
        .build()

    fun getQuote(): QuotesEndpoint = retrofit.create(QuotesEndpoint::class.java)
}
